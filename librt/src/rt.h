#ifndef LIBRT_INTERN_RT_H_
#define LIBRT_INTERN_RT_H_

#include <time.h>

extern struct timespec __librt_monotonic;
extern struct timespec __librt_realtime;

#endif // !LIBRT_INTERN_RT_H_
